#include "game.hpp"
#include <args.hxx>

int main(int argn, char* argv[])
{
	args::ArgumentParser parser("Simple C++ Snake game", "");
	args::HelpFlag help(parser, "help", "Display help", {'h', "help"});
	
	args::ValueFlag<uint16_t> argBonus (parser, "int", "Number of bonus (default: 4)", {'b', "bonus"});
	args::ValueFlag<uint16_t> argLen   (parser, "int", "Snake's initial size (default: 2)", {'l', "length"});
	args::ValueFlag<uint16_t> argSpeed (parser, "int", "Snake's speed (percentage)", {'s', "speed"});
	args::ValueFlag<uint16_t> argHeight(parser, "int", "Window's height (default: 720)", {'h', "height"});
	args::ValueFlag<uint16_t> argWidth (parser, "int", "Window's width (default: 720)", {'w', "width"});
	args::ValueFlag<uint16_t> argGrid  (parser, "int", "Grid's size (default: 20)", {'g', "grid"});
	
	try
	{
		parser.ParseCLI(argn, argv);
	}
	catch (args::Help)
    {
        std::cout << parser;
        return 0;
    }
	catch (args::ParseError e)
	{
	    std::cerr << e.what() << std::endl;
	    std::cerr << parser;
	    return 1;
	}
	
	uint16_t bonus (argBonus  ? args::get(argBonus ) : Game::s_defaultBonus    );
	uint16_t speed (argSpeed  ? args::get(argSpeed ) : Game::s_defaultSpeed    );
	uint16_t len   (argLen    ? args::get(argLen   ) : Game::s_defaultLen      );
	uint16_t height(argHeight ? args::get(argHeight) : Game::s_defaultWinSize.y);
	uint16_t width (argWidth  ? args::get(argWidth ) : Game::s_defaultWinSize.x);
	uint16_t grid  (argGrid   ? args::get(argGrid  ) : Game::s_defaultGridSize );
	                                                                              
	std::cout	<< "bonus:\t"  << bonus  << std::endl
				<< "length:\t" << len    << std::endl
				<< "speed:\t"  << speed  << std::endl
				<< "height:\t" << height << std::endl
				<< "width:\t"  << width  << std::endl
				<< "grid:\t"   << grid   << std::endl << std::endl;
	
	Game game(bonus, len, speed, height, width, grid);
	return game.run();
}
