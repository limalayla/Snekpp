# Snekpp

Unoriginal snake game made in C++ with the [SFML](https://www.sfml-dev.org).

[Game's screenshot](screenshot.png)

## Features
 - Customizable (thanks to the great [Args Library](https://github.com/Taywee/args)!)
 - Lightweight

### Compile and Run
#### Linux
First make sure you have the [Args Library](https://github.com/Taywee/args) installed. Then 
```bash
make && ./snake
```

### Todo
 - [ ] Proper framerate management
 - [x] Multithreading?
