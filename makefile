GPP = g++ -std=c++11 -lpthread

LIB_GRAPHICS = -lsfml-graphics
LIB_SYSTEM   = -lsfml-system
LIB_WINDOW   = -lsfml-window

snake:		  src/main.cpp build/game.o
	$(GPP)    src/main.cpp build/game.o $(LIB_SYSTEM) $(LIB_WINDOW) $(LIB_GRAPHICS) -o snake

build/game.o: build/ src/game.cpp src/game.hpp
	$(GPP) -c src/game.cpp $(LIB_SYSTEM) $(LIB_WINDOW) $(LIB_GRAPHICS) -o build/game.o
	
build/:
	mkdir build/
	
clean:
	rm -rf build/ snake
