#ifndef GAME_HPP
#define GAME_HPP

#include <SFML/Graphics.hpp>
#include <SFML/System.hpp>
#include <iostream>
#include <random>
#include <mutex>

class Game
{
	public:
		Game(uint16_t bonus, uint16_t len, uint16_t pace, uint16_t height, uint16_t width, uint16_t gridSize);

		std::string	load();
		void		init();
		void		update();
		void		draw();
		void		checkEvents();
		int			run();
		void		keyboard(sf::Keyboard::Key);
		void		spawnBonus();

		enum class GameState
		{
			initiating,
			inGame,
			paused,
			error,
			gameOver,
			exiting
		};

		static std::map<sf::Keyboard::Key, sf::Vector2<int8_t>> s_directions;
		static std::map<std::string, sf::Font> s_fonts;
		static sf::Vector2<uint16_t> s_defaultWinSize;
		static const uint16_t s_defaultGridSize = 20;
		static const uint16_t s_defaultBonus = 4;
		static const uint16_t s_defaultSpeed = 100;
		static const uint16_t s_defaultLen = 2;

	protected:
		// Window related
		sf::RenderWindow m_window;
		sf::Event m_events;
		sf::Vector2<uint16_t> m_winSize;
		uint16_t m_gridSize;
		sf::Vector2<uint16_t> m_tileSize;
		
		// Game related
		std::mutex m_board_mutex;
		std::mutex m_state_mutex;
		std::mutex m_direction_mutex;
		
		void sleep();
		void eventsThread();
		void updateThread();
		void drawThread();
		
		std::vector<std::vector<int16_t> > m_board;
		GameState m_state;
		sf::RectangleShape m_tile;
		sf::Vector2<int8_t> m_direction;
		sf::Vector2<uint16_t> m_curPos;
		uint16_t m_lenght;
		uint16_t m_maxBonus;
		uint16_t m_bonus;
		uint16_t m_speed;
		std::default_random_engine m_generator;
		std::uniform_int_distribution<int> m_distribution;

};

#endif // GAME_HPP
