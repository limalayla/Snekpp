#include "game.hpp"
#include <thread>
#include <mutex>
#include <chrono>

std::map<std::string, sf::Font> Game::s_fonts;

sf::Vector2<uint16_t> Game::s_defaultWinSize (720, 720);
std::map<sf::Keyboard::Key, sf::Vector2<int8_t>> Game::s_directions = {
		{sf::Keyboard::Left , sf::Vector2<int8_t>( 0, -1)},
		{sf::Keyboard::Right, sf::Vector2<int8_t>( 0,  1)},
		{sf::Keyboard::Up   , sf::Vector2<int8_t>(-1,  0)},
		{sf::Keyboard::Down , sf::Vector2<int8_t>( 1,  0)}
};

Game::Game(uint16_t bonus, uint16_t len, uint16_t speed, uint16_t height, uint16_t width, uint16_t gridSize)
	:	m_maxBonus(bonus), m_speed(speed), m_winSize(height, width), m_gridSize(gridSize),
		m_window(sf::VideoMode(height, width), "Snake"),
		m_events(),
		m_state(GameState::initiating),
		m_generator(),
		m_distribution(0, m_gridSize-1),
		m_board(m_gridSize, std::vector<int16_t>(m_gridSize, 0)),
		m_tile(),
		m_direction(1, 0),
		m_curPos(m_gridSize / 2, m_gridSize / 2),
		m_lenght(len),
		m_bonus(0),
		m_board_mutex(), m_direction_mutex(), m_state_mutex()
{
	m_window.setVerticalSyncEnabled(false);

	std::string err = load();
	if(err != "")
	{
		std::cerr << "Load Error : " << err << std::endl;
	}

	init();
	m_state = GameState::inGame;
}

std::string Game::load()
{
	std::string problem("");
	//m_fonts["arial"] = sf::Font();

	for(auto i= s_fonts.begin(); i != s_fonts.end(); i++)
	{
		if(!i->second.loadFromFile("font/" + i->first + ".ttf"));
		{
			problem += "Cannot load " + i->first + "\n";
		}
	}

	return problem;
}

void Game::init()
{
	m_tileSize.x = m_winSize.x / m_gridSize;
	m_tileSize.y = m_winSize.y / m_gridSize;
	
	m_tile = sf::RectangleShape(sf::Vector2f(m_tileSize));

	m_tile.setOutlineColor(sf::Color::Black);
	m_tile.setOutlineThickness(0.5);

	m_board[m_curPos.x][m_curPos.y] = m_lenght;

	// Spawn all bonus
	while(m_bonus < m_maxBonus)
		spawnBonus();
}

void Game::checkEvents()
{
	while(m_window.pollEvent(m_events))
	{
		switch (m_events.type)
		{
			case sf::Event::Closed:
				m_window.close();
				m_state_mutex.lock();
				m_state = GameState::exiting;
				m_state_mutex.unlock();
				break;

			case sf::Event::KeyPressed:
				keyboard(m_events.key.code);
				break;

			default:
				break;
		}
	}
}

void Game::keyboard(sf::Keyboard::Key keyPressed)
{
	switch (keyPressed)
	{
		case sf::Keyboard::Escape:
			m_window.close();
			m_state_mutex.lock();
			m_state = GameState::exiting;
			m_state_mutex.unlock();
			break;

		case sf::Keyboard::Up:
		case sf::Keyboard::Down:
		case sf::Keyboard::Left:
		case sf::Keyboard::Right:
			m_direction_mutex.lock();
			if(s_directions[keyPressed].x != -m_direction.x || s_directions[keyPressed].y != -m_direction.y)
				m_direction = s_directions[keyPressed];
			m_direction_mutex.unlock();
			break;
			
		default:
			break;
	}
}

void Game::spawnBonus()
{
	int x, y;

	do
	{
		x = m_distribution(m_generator);
		y = m_distribution(m_generator);
	}
	while(m_board[x][y] != 0);
	
	m_board[x][y] = -1;
	m_bonus++;
}

void Game::update()
{
	m_state_mutex.lock();
	if(m_state == GameState::gameOver)
	{
		std::cout << std::endl << std::endl<< "Game Over :(" << std::endl << "Final score: " << m_lenght << std::endl;
		m_state = GameState::exiting;
		m_state_mutex.unlock();
		return;
	}
	m_state_mutex.unlock();
	
	m_direction_mutex.lock();
	sf::Vector2<uint16_t> newPos(m_curPos.x + m_direction.x, m_curPos.y + m_direction.y);
	m_direction_mutex.unlock();
	
	if(newPos.x == 65535) newPos.x = m_gridSize-1;
	if(newPos.y == 65535) newPos.y = m_gridSize-1;
	if(newPos.x == m_gridSize) newPos.x = 0;
	if(newPos.y == m_gridSize) newPos.y = 0;
	
	std::lock_guard<std::mutex> guard(m_board_mutex);
	if(m_board[newPos.x][newPos.y] > 0) m_state = GameState::gameOver;
	else
	{
		if(m_board[newPos.x][newPos.y] < 0)
		{
			m_lenght++;
			m_bonus--;
		}
		
		m_board[newPos.x][newPos.y] = m_lenght + 1;
		m_curPos = newPos;
	}

	if(m_bonus < m_maxBonus)
		spawnBonus();

	for(auto y= m_board.begin(); y != m_board.end(); y++)
		std::for_each(y->begin(), y->end(), [](int16_t& x){ if(x > 0) x--; });
	
	//std::cout << m_curPos.x << ", " << m_curPos.y << "  :  " << m_lenght << std::endl;
}

void Game::draw()
{
	m_window.clear(sf::Color::Black);
	m_tile.setPosition((m_winSize.x - m_tileSize.x * m_gridSize) / 2,
					   (m_winSize.y - m_tileSize.y * m_gridSize) / 2);
	
	auto tileColor = [](int16_t x)
	{
		     if(x == 0) return sf::Color::White;
		else if(x >  0) return sf::Color::Green;
		else            return sf::Color::Red;
	};
	
	m_board_mutex.lock();
	for(auto y= m_board.cbegin(); y != m_board.cend(); y++)
	{
		for(auto x= y->cbegin(); x != y->cend(); x++)
		{
			m_tile.setFillColor(tileColor(*x));

			m_window.draw(m_tile);
			m_tile.move(m_tileSize.x, 0);
		}

		m_tile.move(- m_gridSize * m_tileSize.x, m_tileSize.y);
	}
	m_board_mutex.unlock();

	m_window.display();
}

void Game::sleep()
{
	long int x = 50 * (100.0/m_speed);
	std::this_thread::sleep_for(std::chrono::milliseconds(x));
}

void Game::eventsThread()
{
	while(true)
	{
		m_state_mutex.lock();
		if(m_state == GameState::exiting) break;
		m_state_mutex.unlock();
		
		checkEvents();
		//sf::sleep(sf::milliseconds(50 * (100.0/m_pace)));
		Game::sleep();
	}
	
	m_state_mutex.unlock();
	std::cout << "end of events thread" << std::endl;
}

void Game::updateThread()
{
	while(true)
	{
		m_state_mutex.lock();
		if(m_state == GameState::exiting) break;
		m_state_mutex.unlock();
		
		update();
		Game::sleep();
	}
	
	m_state_mutex.unlock();
	std::cout << "end of update thread" << std::endl;
}

void Game::drawThread()
{
	while(true)
	{
		m_state_mutex.lock();
		if(m_state == GameState::exiting) break;
		m_state_mutex.unlock();
		
		draw();
		Game::sleep();
	}
	
	m_state_mutex.unlock();
	std::cout << "end of draw thread" << std::endl;
}

int Game::run()
{
	std::thread eventTh(&Game::eventsThread, this);
	std::thread updateTh(&Game::updateThread, this);
	std::thread drawTh(&Game::drawThread, this);

	eventTh.join();
	updateTh.join();
	drawTh.join();
	
	return 0;
}
